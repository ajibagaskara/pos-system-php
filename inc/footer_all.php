 <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Inventory|POS Ver. 2.0
    </div>
    <!-- Default to the left -->
    <strong>Toko Hussein</a>.</strong> Jl. Jakarta Ruko No.1
  </footer>
</div>
<!-- ./wrapper -->
<!-- Select2 -->
<script src="plugins/select2/dist/js/select2.min.js"></script>
<!-- DataTables -->
<script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

</body>
</html>