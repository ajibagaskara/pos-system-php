<?php
    include_once'db/connect_db.php';
    session_start();
    if($_SESSION['username']==""){
      header('location:index.php');
    }else{
      if($_SESSION['role']=="Admin"){
        include_once'inc/header_all.php';
      }else{
          include_once'inc/header_all_operator.php';
      }
    }
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content container-fluid">
      <div class="row">
          <!-- get alert stock -->
          <?php
          $select = $pdo->prepare("SELECT count(product_code) as total FROM
          tbl_product WHERE stock <= min_stock");
          $select->execute();
          $row=$select->fetch(PDO::FETCH_OBJ);
          $total1 = $row->total;
          ?>
        <!-- get alert notification -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-archive"></i></span>
            <div class="info-box-content">
              <span class="text-muted">Produk Menipis</span>
              <?php if($total1==true){ ?>
              <span class="info-box-text label label-danger"><i class="fa fa-warning"> <small><?php echo $row->total;?></small></i></span>
              <?php }else{?>
              <span class="info-box-text label label-success"><i class="fa fa-check-circle"> <strong>0</strong></i></span>
              <?php }?>
            </div>
            <div class="info-box-content">
              <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#exampleModalCenter">
                <span><i class="fa fa-info-circle"></i> Info</span>
              </button>
            </div>

            <!-- Button trigger modal -->


          <!-- Modal List Stock -->
          <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <h5 class="modal-title" id="exampleModalLongTitle">Daftar Persediaan</h5>
                        </div>
                        <div class="modal-body">
                          <table class="table table-striped">
                            <thead>
                              <tr>
                                  <th>No</th>
                                  <th>Produk</th>
                                  <th>Kode</th>
                                  <th>Kategori</th>
                                  <th>Persediaan</th>
                                  <th>Satuan</th>
                                  <th>Opsi</th>
                              </tr>
                            </thead>
                            <tbody>
                                <?php
                                  $no = 1;
                                  $select = $pdo->prepare("SELECT product_id,product_code,product_name,product_category,stock,product_satuan FROM tbl_product
                                  WHERE stock <= min_stock");
                                  $select->execute();
                                  while($row=$select->fetch(PDO::FETCH_OBJ)){
                                ?>
                                    <tr>
                                    <td><?php echo $no++ ;?></td>
                                    <td><?php echo $row->product_name; ?></td>
                                    <td><?php echo $row->product_code; ?></td>
                                    <td><?php echo $row->product_category; ?></td>
                                    <td><?php echo $row->stock; ?></td>
                                    <td><?php echo $row->product_satuan; ?></td>
                                    <td>
                                    <a href="edit_product.php?id=<?php echo $row->product_id; ?>"
                                    class="btn btn-info btn-sm"><i class="fa fa-pencil"></i></a>
                                    </td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                          </table>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        </div>
                        </div>
                      </div>
            </div>
                <!-- /.info-box-content -->
          </div>

          <!-- /.info-box -->
        </div>

        <!-- get total products-->
        <?php
        $select = $pdo->prepare("SELECT count(product_code) as t FROM tbl_product");
        $select->execute();
        $row=$select->fetch(PDO::FETCH_OBJ);
        $total = $row->t;
        ?>

        <!-- get total products notification -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-cubes"></i></span>
            <div class="info-box-content">
              <span class="text-muted">Total Produk</span>
              <?php
              if($total==true){
              ?>
              <span class="info-box-text label label-success"><small><?php echo $row->t ?></small></span>
              <?php } else {?>
              <span class="info-box-text label label-warning"><small>Belum Ada Produk</small></span>
              <?php
              }
              ?>
            </div>
            <div class="info-box-content">
              <a href="product.php" class="btn btn-info btn-sm"><span><i class="fa fa-eye"></i>
              </span>Lihat</a>
            </div>
          </div>
        </div>

        <!-- get today transactions -->
        <?php
        $select = $pdo->prepare("SELECT count(invoice_id) as i FROM tbl_invoice WHERE order_date = CURDATE()");
        $select->execute();
        $row=$select->fetch(PDO::FETCH_OBJ);
        $invoice = $row->i ;
        ?>
         <!-- get today transactions notification -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-shopping-cart"></i></span>

            <div class="info-box-content">
              <span class="text-muted">Penjualan Hari Ini</span>
              <?php
              if($invoice==true){
              ?>
              <span class="info-box-text  label label-success"><small><?php echo $row->i ?></small></span>
              <?php
              } else{
              ?>
              <span class="info-box-text  label label-default"><small>-</small></span>
              <?php
              }
              ?>
            </div>
            <div class="info-box-content">
              <a href="create_order.php" class="btn btn-success btn-sm"><span><i class="fa fa-plus"></i></span>
              Tambah Transaksi</a>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>


        <!-- get today income -->
        <?php
        $select = $pdo->prepare("SELECT sum(total) as total FROM tbl_invoice WHERE order_date = CURDATE()");
        $select->execute();
        $row=$select->fetch(PDO::FETCH_OBJ);
        $total = $row->total ;
        ?>
         <!-- get today income -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-money"></i></span>

            <div class="info-box-content">
              <span class="text-muted">Pendapatan Hari Ini</span>
              <?php
              if($total == true) {
              ?>
              <span class="info-box-number  label label-success"><small>Rp. <?php echo number_format($total,0); ?></small></span>
              <?php
              } else{
              ?>
              <span class="info-box-text  label label-default"><small>-</small></span>
              <?php
              }
              ?>

            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

      </div>

      <div class="col-md-offset-1 col-md-10">
        <div class="col-md-6">
          <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Produk Terlaku</h3>
            </div>
            <div class="box-body">
                <div style="overflow-x:auto;">
                    <table class="table table-striped" id="myBestProduct">
                        <thead>
                            <tr>
                                <th style="width:20px;">No</th>
                                <th style="width:400px;">Kode</th>
                                <th style="width:500px;">Produk</th>
                                <th>Model</th>
                                <th style="width:300px;">Kategori</th>
                                <th style="width:60px;">Terjual</th>
                            </tr>

                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $select = $pdo->prepare("SELECT tbl_product.product_code, tbl_product.product_name, tbl_product.product_category,tbl_product.description,
                            tbl_invoice_detail.qty, tbl_invoice_detail.product_satuan, tbl_invoice_detail.price,
                            sum(qty) as q FROM tbl_product, tbl_invoice_detail WHERE tbl_product.product_id = tbl_invoice_detail.product_id
                            GROUP BY product_code  ORDER BY sum(qty) DESC LIMIT 30");
                            $select->execute();
                            while($row=$select->fetch(PDO::FETCH_OBJ)){
                            ?>
                                <tr>
                                <td><?php echo $no++ ;?></td>
                                <td><?php echo $row->product_code; ?></td>
                                <td><?php echo $row->product_name; ?></td>
                                <td><?php echo $row->description; ?></td>
                                <td><?php echo $row->product_category; ?></td>
                                <td><?php echo $row->q; ?>
                                <span><?php echo $row->product_satuan; ?></span></td>
                                </tr>
                          <?php
                            }
                          ?>
                        </tbody>
                    </table>
                </div>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Penjualan Hari Ini</h3>
            </div>
            <div class="box-body">
                <div style="overflow-x:auto;">
                    <table class="table table-striped" id="myOrderList">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode</th>
                                <th>Produk</th>
                                <th>Model</th>
                                <th>Terjual</th>
                                <th>Pendapatan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no ="1";
                            $select = $pdo->prepare("SELECT tbl_invoice.order_date, tbl_invoice_detail.product_code, tbl_invoice_detail.product_name, SUM(tbl_invoice_detail.qty) as q, SUM(tbl_invoice_detail.total) as total,
                            tbl_invoice_detail.product_satuan, tbl_invoice_detail.descrip FROM tbl_invoice JOIN tbl_invoice_detail ON tbl_invoice.invoice_id =
                            tbl_invoice_detail.invoice_id WHERE order_date = CURDATE()");
                            $select->execute();
                            while($row=$select->fetch(PDO::FETCH_OBJ)){
                            ?>
                                <tr>
                                <td><?php echo $no++ ; ?></td>
                                <td><?php echo $row->product_code; ?></td>
                                <td><?php echo $row->product_name; ?></td>
                                <td><?php echo $row->descrip; ?></td>
                                <td><?php echo $row->q; ?> <span><?php echo $row->product_satuan; ?></span></td>
                                <td>Rp. <?php echo number_format($row->total); ?></td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div><!-- div tabel -->
            </div><!-- box body -->
          </div>
        </div>

      </div>
    </section><!-- Section Container -->
  </div><!--content-wrapper-->

    <!-- data table plugin -->
    <script>
      $(document).ready( function () {
          $('#myBestProduct').DataTable();
      });
    </script>

    <!-- data table plugin -->
    <script>
      $(document).ready( function () {
          $('#myOrderList').DataTable();
      });
    </script>
 <?php
    include_once'inc/footer_all.php';
 ?>