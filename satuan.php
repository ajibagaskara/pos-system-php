<?php
include_once'db/connect_db.php';
session_start();
if($_SESSION['username']==""){
  header('location:index.php');
}else{
  if($_SESSION['role']=="Admin"){
    include_once'inc/header_all.php';
  }else{
      include_once'inc/header_all_operator.php';
  }
}

if(isset($_POST['submit'])){
    $satuan = $_POST['satuan'];
    if(isset($_POST['satuan'])){

            $select = $pdo->prepare("SELECT nm_satuan FROM tbl_satuan WHERE nm_satuan='$satuan'");
            $select->execute();

            if($select->rowCount() > 0 ){
                echo'<script type="text/javascript">
                    jQuery(function validation(){
                    Swal.fire("Warning", "Satuan Telah Ada", "warning", {
                    button: "Continue",
                        });
                    });
                    </script>';
                }else{
                    $insert = $pdo->prepare("INSERT INTO tbl_satuan(nm_satuan) VALUES(:satuan)");

                    $insert->bindParam(':satuan', $satuan);

                    if($insert->execute()){
                        echo '<script type="text/javascript">
                        jQuery(function validation(){
                        Swal.fire("Success", "Satuan Baru Telah Dibuat", "success", {
                        button: "Continue",
                            });
                        });
                        </script>';
                        }
                }
    }
}

?>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content container-fluid">

      <div class="col-md-offset-1 col-md-10">
        <!-- Category Form-->
        <div class="col-md-4">
              <div class="box box-success">
                  <!-- /.box-header -->
                  <!-- form start -->
                  <form action="" method="POST">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="category">Nama Satuan</label>
                        <input type="text" class="form-control" name="satuan" placeholder="Masukan Satuan" required>
                      </div>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success" name="submit"><i class="fa fa-plus"></i> Tambah</button>
                    </div>
                  </form>
              </div>
        </div>
          <!-- Category Table -->
        <div class="col-md-8">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Daftar Satuan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="overflow-x:auto;">
              <table class="table table-striped" id="">
                  <thead>
                      <tr>
                          <th>No</th>
                          <th>Nama Satuan</th>
                          <th>Opsi</th>
                      </tr>

                  </thead>
                  <tbody>
                  <?php
                  $no = 1;
                  $select = $pdo->prepare('SELECT * FROM tbl_satuan');
                  $select->execute();
                  while($row=$select->fetch(PDO::FETCH_OBJ)){ ?>
                    <tr>
                      <td><?php echo $no ++ ?></td>
                      <td><?php echo $row->nm_satuan; ?></td>
                      <td>
                          <a href="edit_satuan.php?id=<?php echo $row->kd_satuan; ?>"
                          class="btn btn-info btn-sm" name="btn_edit"><i class="fa fa-pencil"></i></a>
                          <a href="delete_satuan.php?id=<?php echo $row->kd_satuan; ?>"
                          class="btn btn-danger btn-sm btn-del" name="btn_delete"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                  <?php
                  }
                  ?>

                  </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script>
    $('.btn-del').on('click', function(e){
      e.preventDefault();
      const href = $(this).attr('href')

      Swal.fire({
          title : 'Anda Yakin?',
          text : 'Setelah Dihapus Data Tidak Akan Bisa Kembali',
          icon : 'warning',
          showCancelButton : true,
          confirmButtonColor : '#3085d6',
          cancelButtonColor : '#d33',
          confirmButtonText : 'Hapus Satuan',
          cancelButtonText : 'Tidak'
      }).then((result) =>{
          if(result.value){
            document.location.href=href;
          }
      })
    })
  </script>


  <!-- DataTables Function -->
  <script>
  $(document).ready( function () {
      $('#mySatuan').DataTable();
  } );
  </script>

<?php
  include_once'inc/footer_all.php';
?>