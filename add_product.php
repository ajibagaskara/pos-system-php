<?php
   include_once'db/connect_db.php';
   session_start();
   if($_SESSION['username']==""){
     header('location:index.php');
   }else{
     if($_SESSION['role']=="Admin"){
       include_once'inc/header_all.php';
     }else{
         include_once'inc/header_all_operator.php';
     }
   }

   $select = $pdo->prepare("SELECT max(product_code) as maxId FROM tbl_product");
   $select->execute();
   $result = $select->fetch();
   $kodeBarang = $result['maxId'];

   $noUrut = (int) substr($kodeBarang,3,3 );
   $noUrut++;
   $char = "PRD";
   $kodeBarang = $char.sprintf("%03s",$noUrut);

    if(isset($_POST['add_product'])){
        $code = $_POST['product_code'];
        $product = $_POST['product_name'];
        $category = $_POST['category'];
        $purchase = $_POST['purchase_price'];
        $sell = $_POST['sell_price'];
        $stock = $_POST['stock'];
        $min_stock = $_POST['min_stock'];
        $satuan = $_POST['satuan'];
        $desc = $_POST['description'];


        if(isset($_POST['product_code'])){
            $select = $pdo->prepare("SELECT product_code FROM tbl_product WHERE product_code='$code'");
            $select->execute();

            if($select->rowCount() > 0 ){
                echo'<script type="text/javascript">
                    jQuery(function validation(){
                    Swal.fire("Warning", "Kode Produk Sudah Terdaftar", "warning", {
                    button: "Continue",
                        });
                    });
                    </script>';

            }else{
                    $img = $_FILES['product_img']['name'];
                    if(!empty($img)){
                        $img_tmp = $_FILES['product_img']['tmp_name'];
                        $img_size = $_FILES['product_img']['size'];
                        $img_ext = explode('.', $img);
                        $img_ext = strtolower(end($img_ext));
                        $img_new = uniqid().'.'. $img_ext;
                        $store = "upload/".$img_new;
                        if($img_ext == 'jpg' || $img_ext == 'jpeg' || $img_ext == 'png' || $img_ext == 'gif'){
                            if($img_size>= 1000000){
                                $error ='<script type="text/javascript">
                                        jQuery(function validation(){
                                        Swal.fire("Error", "File Should Be 1MB", "error", {
                                        button: "Continue",
                                            });
                                        });
                                        </script>';
                                echo $error;
                            }else{
                                if(move_uploaded_file($img_tmp,$store)){
                                    $product_img = $img_new;
                                    if(!isset($error)){

                                        $insert = $pdo->prepare("INSERT INTO tbl_product(product_code,product_name,product_category,purchase_price,sell_price,stock,min_stock,product_satuan,description,img)
                                        values(:product_code,:product_name,:product_category,:purchase_price,:sell_price,:stock,:min_stock,:satuan,:desc,:img)");

                                        $insert->bindParam(':product_code', $code);
                                        $insert->bindParam(':product_name', $product);
                                        $insert->bindParam(':product_category', $category);
                                        $insert->bindParam(':purchase_price', $purchase);
                                        $insert->bindParam(':sell_price', $sell);
                                        $insert->bindParam(':stock', $stock);
                                        $insert->bindParam(':min_stock', $min_stock);
                                        $insert->bindParam(':satuan', $satuan);
                                        $insert->bindParam(':desc', $desc);
                                        $insert->bindParam(':img', $product_img);

                                        if($insert->execute()){
                                            $message = 'success';
                                            echo "<meta http-equiv='refresh' content='2'>";
                                        }else{
                                            $error = 'error';
                                        }

                                    }else{
                                        echo '<script type="text/javascript">
                                                    jQuery(function validation(){
                                                    Swal.fire("Error", "Terjadi Kesalahan", "error", {
                                                    button: "Continue",
                                                        });
                                                    });
                                                    </script>';;;
                                    }
                                }

                            }
                        }else{
                            $error = '<script type="text/javascript">
                            jQuery(function validation(){
                            Swal.fire("Error", "Tolong Upload Gambar Dengan Format : jpg, jpeg, png, gif", "error", {
                            button: "Continue",
                                });
                            });
                            </script>';
                            echo $error;

                        }

                    }else{
                        $img = 'no-image.png';
                        $insert = $pdo->prepare("INSERT INTO tbl_product(product_code,product_name,product_category,purchase_price,sell_price,stock,min_stock,product_satuan,description,img)
                        values(:product_code,:product_name,:product_category,:purchase_price,:sell_price,:stock,:min_stock,:satuan,:desc,:img)");

                        $insert->bindParam(':product_code', $code);
                        $insert->bindParam(':product_name', $product);
                        $insert->bindParam(':product_category', $category);
                        $insert->bindParam(':purchase_price', $purchase);
                        $insert->bindParam(':sell_price', $sell);
                        $insert->bindParam(':stock', $stock);
                        $insert->bindParam(':min_stock', $min_stock);
                        $insert->bindParam(':satuan', $satuan);
                        $insert->bindParam(':desc', $desc);
                        $insert->bindParam(':img', $img);

                        if($insert->execute()){
                            $message = 'success';
                            echo "<meta http-equiv='refresh' content='2'>";
                        }else{
                            $errormsg = 'error';
                        }

                    }

                }
        }

    }
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content container-fluid">
        <div class="col-md-offset-1 col-md-10">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Masukan Produk Baru</h3>
                </div>
                <form action="" method="POST"
                    enctype="multipart/form-data" autocomplete="off">
                    <div class="box-body">

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Kode Produk</label><br>
                                <span class="text-muted">*Kode Produk Terisi Otomatis</span>
                                <input type="text" class="form-control"
                                name="product_code" value="<?php echo $kodeBarang ?>" readonly>
                            </div>
                            <div class="form-group">
                                <label for="">Nama Produk</label><br><br>
                                <input type="text" class="form-control"
                                name="product_name">
                            </div>
                            <div class="form-group">
                                <label for="description">Model Produk</label><br><br>
                                <input type="text" name="description" id="description"
                                 class="form-control" required>
                            </div>



                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Persediaan</label><br>
                                <span class="text-muted">*Sesuaikan Dengan Persediaan Gudang</span>
                                <input type="number" min="1" step="1"
                                class="form-control" name="stock" required>
                            </div>

                            <div class="form-group">
                                <label for="">Persediaan Minimal</label><br><br>
                                <input type="number" min="1" step="1"
                                class="form-control" name="min_stock" required>
                            </div>

                            <div class="form-group">
                                <label for="">Satuan</label><br><br>
                                <select class="form-control" name="satuan" required>
                                    <?php
                                    $select = $pdo->prepare("SELECT * FROM tbl_satuan");
                                    $select->execute();
                                    while($row = $select->fetch(PDO::FETCH_ASSOC)){
                                        extract($row)
                                    ?>
                                        <option><?php echo $row['nm_satuan']; ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Kategori</label><br><br>
                                <select class="form-control" name="category" required>
                                    <?php
                                    $select = $pdo->prepare("SELECT * FROM tbl_category");
                                    $select->execute();
                                    while($row = $select->fetch(PDO::FETCH_ASSOC)){
                                        extract($row)
                                    ?>
                                        <option><?php echo $row['cat_name']; ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Harga Modal</label><br><br>
                                <input type="number" min="1000" step="1000"class="form-control"
                                name="purchase_price" required>
                            </div>
                            <div class="form-group">
                                <label>Harga Jual</label><br><br>
                                <input type="number" class="form-control"
                                name="sell_price" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="">Gambar Produk</label><br>
                            <br>
                            <input type="file" class="input-group"
                            name="product_img" onchange="readURL(this);"> <br>
                            <img id="img_preview" src="upload/<?php echo $row->img?>" alt="Preview" class="img-responsive" />
                        </div>
                    </div>

                    <div class="box-footer" align="center">
                        <button type="submit" class="btn btn-success"
                        name="add_product"> <i class="fa fa-check-circle"></i> Tambah</button>
                        <a href="product.php" class="btn btn-danger"> <i class="fa fa-reply"></i> </a>
                    </div>

                    <?php
                        if(!empty($message))
                        {
                            echo'<script type="text/javascript">
                                jQuery(function validation(){
                                Swal.fire("Success", "Produk Berhasil Disimpan", "success", {
                                button: "Continue",
                                    });
                                });
                                </script>';
                        }
                        if(empty($error)){}
                        else{
                            echo '<script type="text/javascript">
                                jQuery(function validation(){
                                Swal.fire("Error", "Terjadi Kesalahan", "error", {
                                button: "Continue",
                                    });
                                });
                                </script>';
                        }
                    ?>

                </form>
            </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#img_preview').attr('src', e.target.result)
                .width(250)
                .height(200);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

 <?php
    include_once'inc/footer_all.php';
 ?>